#!/bin/bash

rm -rf DOSBOX
rm -rf Documentation
rm -rf "Video Codec"
rm GameuxInstallHelper.dll
rm EULA.txt
rm gog*.*
rm *.lnk
rm *.ico
rm unins0*.*
rm webcache.zip