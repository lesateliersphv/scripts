#!/bin/bash

mkdir out

for i in *.m4a ; do 
    ffmpeg -i "$i" cover.jpg
    ffmpeg -i "$i" -acodec flac ./out/"$(basename "${i/.m4a}")".flac
    metaflac --import-picture-from=./cover.jpg ./out/"$(basename "${i/.m4a}")".flac
    rm cover.jpg
    sleep 5
done